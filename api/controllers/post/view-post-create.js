module.exports = {


  friendlyName: 'View post create',


  description: 'Display "Post create" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/post/post-create'
    }

  },


  fn: async function (inputs, exits) {

    // Respond with view.
    return exits.success();

  }


};
