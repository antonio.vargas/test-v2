/**
 * PostController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  inputs: {

    postContent: {
      required: true,
      type: 'string',
    }
  },

  exits: {
    invalid: {
      responseType: 'badRequest',
      description: 'The provided post content are invalid',
    }
  },

  fn: async function (inputs, exits) {
    var newPostRecord = await Post.create(Object.asssign({
      postContent: inputs.postContent
    }))
    .intercept({name: 'UsageError'}, 'invalid')
    .fetch();

    return exits.success();
  }
};
